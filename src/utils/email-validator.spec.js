// Quando o Jest está instalado e existe possibilidade de criar pasta "__mocks__" na raíz do projeto, todo require puxa os arquivos dessa
// pasta, por isso o pacote do npm "validate" não é chamado aqui e sim o mock "__mocks__/validator.js". Isso foi feito aqui por que o
// objetivo do teste não é validar a chamada do pacote validator e sim o comportamento da classe EmailValidator quando se passa um email
// válido ou inválido
const validator = require('validator')
const EmailValidator = require('./email-validator')
const { ServerError } = require('./errors')

const makesut = () => {
  return new EmailValidator()
}

describe('Email Validator', () => {
  test('Should return true if validator returns true ', () => {
    const sut = makesut()
    const isEmailValid = sut.isValid('valid_email@email.com')
    expect(isEmailValid).toBe(true)
  })
  test('Should return false if validator returns false ', () => {
    const sut = makesut()
    validator.isEmailValid = false
    const isEmailValid = sut.isValid('invalid_email@email.com')
    expect(isEmailValid).toBe(false)
  })
  test('Should call validator with correct email', () => {
    const sut = makesut()
    sut.isValid('any_email@email.com')
    expect(validator.email).toBe('any_email@email.com')
  })
  test('Should throw if no email is provided', () => {
    const sut = makesut()
    expect(() => { sut.isValid() }).toThrow(new ServerError())
  })
})
