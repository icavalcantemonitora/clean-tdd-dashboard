const Encrypter = require('./encrypter')
const { ServerError } = require('./errors')
const bcrypt = require('bcrypt')

const makeSut = () => {
  return new Encrypter()
}

describe('Encrypter', () => {
  test('Should return true if bcrypt returns true', async () => {
    const sut = makeSut()

    const isValid = await sut.compare('any_value', 'hashed_value')
    expect(isValid).toBe(true)
  })
  test('Should return false if bcrypt returns false', async () => {
    const sut = makeSut()
    bcrypt.isValid = false
    const isValid = await sut.compare('any_value', 'hashed_value')
    expect(isValid).toBe(false)
  })
  test('Should call bcrypt with correct values', async () => {
    const sut = makeSut()
    await sut.compare('any_value', 'hashed_value')
    expect(bcrypt.value).toBe('any_value')
    expect(bcrypt.hashedValue).toBe('hashed_value')
  })
  test('Should throw if no value if provided to bcrypt', async () => {
    const sut = makeSut()
    const promise = sut.compare()
    await expect(promise).rejects.toThrow(new ServerError())
  })
  test('Should throw if no hashedValue if provided to bcrypt', async () => {
    const sut = makeSut()
    const promise = sut.compare('any_value')
    await expect(promise).rejects.toThrow(new ServerError())
  })
})
