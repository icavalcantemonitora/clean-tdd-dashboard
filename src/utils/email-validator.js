const validator = require('validator')
const { ServerError } = require('./errors')

class EmailValidator {
  isValid (email) {
    if (!email) {
      throw new ServerError()
    }
    return validator.isEmail(email)
  }
}

module.exports = EmailValidator
