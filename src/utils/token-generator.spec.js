const TokenGenerator = require('./token-generator')
const { ServerError } = require('./errors')
const jwt = require('jsonwebtoken')

const makeSut = () => {
  return new TokenGenerator('secret')
}

const makeSutWithError = () => {
  return new TokenGenerator()
}

describe('TokenGenerator', () => {
  test('Should return null if JWT returns null', async () => {
    const sut = makeSut()
    jwt.token = null
    const token = await sut.generate('any_id')
    expect(token).toBeNull()
  })

  test('Should return a token if JWT returns a token', async () => {
    const sut = makeSut()
    const token = await sut.generate('any_id')
    expect(token).toBe(jwt.token)
  })

  test('Should call JWT with correct values', async () => {
    const sut = makeSut()
    await sut.generate('any_id')
    expect(jwt.id).toBe('any_id')
    expect(jwt.secret).toBe(sut.secret)
  })
  test('Should throw if no id if provided to jwt', async () => {
    const sut = makeSut()
    const promise = sut.generate()
    await expect(promise).rejects.toThrow(new ServerError())
  })
  test('Should throw if no secret if provided to jwt', async () => {
    const sut = makeSutWithError()
    const promise = sut.generate('any_id')
    await expect(promise).rejects.toThrow(new ServerError())
  })
})
