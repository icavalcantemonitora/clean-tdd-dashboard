const jwt = require('jsonwebtoken')
const { ServerError } = require('./errors')

class TokenGenerator {
  constructor (secret) {
    this.secret = secret
  }

  async generate (id) {
    if (!id) {
      throw new ServerError()
    }
    if (!this.secret) {
      throw new ServerError()
    }
    return jwt.sign(id, this.secret)
  }
}
module.exports = TokenGenerator
