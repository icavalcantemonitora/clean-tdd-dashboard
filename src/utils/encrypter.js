const bcrypt = require('bcrypt')
const { ServerError } = require('./errors')

class Encrypter {
  async compare (value, hash) {
    if (!value) {
      throw new ServerError()
    }
    if (!hash) {
      throw new ServerError()
    }
    const isValid = await bcrypt.compare(value, hash)
    return isValid
  }
}

module.exports = Encrypter
