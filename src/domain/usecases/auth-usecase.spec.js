const { MissingParamError, ServerError } = require('../../utils/errors')
const AuthUseCase = require('./auth-usecase')

const makeSut = () => {
  const loadUserByEmailRepositorySpy = makeLoadUserByEmailRepository()
  const encrypterSpy = makeEncrypter()
  const tokenGeneratorSpy = makeTokenGenerator()
  const updateAccessTokenRepositorySpy = makeUpdateAccessTokenRepository()

  loadUserByEmailRepositorySpy.user = {
    id: 'anyId',
    password: 'hashed_password'
  }
  encrypterSpy.isValid = true
  tokenGeneratorSpy.accessToken = 'anyToken'

  const sut = new AuthUseCase({
    loadUserByEmailRepository: loadUserByEmailRepositorySpy,
    encrypter: encrypterSpy,
    tokenGenerator: tokenGeneratorSpy,
    updateAccessTokenRepository: updateAccessTokenRepositorySpy
  })
  return {
    sut,
    loadUserByEmailRepositorySpy,
    encrypterSpy,
    tokenGeneratorSpy,
    updateAccessTokenRepositorySpy
  }
}

const makeLoadUserByEmailRepository = () => {
  class LoadUserByEmailRepositorySpy {
    async load (email) {
      this.email = email
      return this.user
    }
  }
  return new LoadUserByEmailRepositorySpy()
}

const makeLoadUserByEmailRepositoryWithError = () => {
  class LoadUserByEmailRepositorySpy {
    async load (email) {
      throw new Error()
    }
  }
  return new LoadUserByEmailRepositorySpy()
}

const makeEncrypter = () => {
  class EncrypterSpy {
    async compare (password, hashedPassword) {
      this.password = password
      this.hashedPassword = hashedPassword
      return this.isValid
    }
  }
  return new EncrypterSpy()
}

const makeEncrypterWithError = () => {
  class EncrypterSpy {
    async compare (password, hashedPassword) {
      throw new Error()
    }
  }
  return new EncrypterSpy()
}

const makeTokenGenerator = () => {
  class TokenGeneratorSpy {
    async generate (userId) {
      this.userId = userId
      return this.accessToken
    }
  }
  return new TokenGeneratorSpy()
}

const makeTokenGeneratorWithError = () => {
  class TokenGeneratorSpy {
    async generate (userId) {
      throw new Error()
    }
  }
  return new TokenGeneratorSpy()
}

const makeUpdateAccessTokenRepository = () => {
  class UpdateAccessTokenRepository {
    async update (userId, accessToken) {
      this.userId = userId
      this.accessToken = accessToken
    }
  }
  return new UpdateAccessTokenRepository()
}

const makeUpdateAccessTokenRepositoryWithError = () => {
  class UpdateAccessTokenRepository {
    async update (userId, accessToken) {
      throw new Error()
    }
  }
  return new UpdateAccessTokenRepository()
}

describe('Auth UseCase', () => {
  test('Should throw if no email is provided', async () => {
    const { sut } = makeSut()
    const promise = sut.auth()
    await expect(promise).rejects.toThrow(new MissingParamError('email'))
  })

  test('Should throw if no password is provided', async () => {
    const { sut } = makeSut()
    const promise = sut.auth('anyemail@email.com')
    await expect(promise).rejects.toThrow(new MissingParamError('password'))
  })

  test('Should call LoadUserByEmailRepository with correct email', async () => {
    const { sut, loadUserByEmailRepositorySpy } = makeSut()
    sut.auth('anyemail@email.com', 'anythingHere')
    expect(loadUserByEmailRepositorySpy.email).toBe('anyemail@email.com')
  })

  test('Should return null if an invalid email is provided', async () => {
    const { sut, loadUserByEmailRepositorySpy } = makeSut()
    loadUserByEmailRepositorySpy.user = null
    const accessToken = await sut.auth('invalid@email.com', 'anythingHere')
    expect(accessToken).toBeNull()
  })

  test('Should return null if an wrong password is provided', async () => {
    const { sut, encrypterSpy } = makeSut()
    encrypterSpy.isValid = false
    const accessToken = await sut.auth('valid@email.com', 'anythingHere')
    expect(accessToken).toBeNull()
  })

  test('Should call Encrypter with correct values', async () => {
    const { sut, loadUserByEmailRepositorySpy, encrypterSpy } = makeSut()
    await sut.auth('valid@email.com', 'anythingHere')
    expect(encrypterSpy.password).toBe('anythingHere')
    expect(encrypterSpy.hashedPassword).toBe(loadUserByEmailRepositorySpy.user.password)
  })

  test('Should call TokenGenerator with correct userId', async () => {
    const { sut, loadUserByEmailRepositorySpy, tokenGeneratorSpy } = makeSut()
    await sut.auth('valid@email.com', 'anythingHere')
    expect(tokenGeneratorSpy.userId).toBe(loadUserByEmailRepositorySpy.user.id)
  })

  test('Should return an accessToken if correct credentials are provided', async () => {
    const { sut, tokenGeneratorSpy } = makeSut()
    const accessToken = await sut.auth('valid@email.com', 'anythingHere')
    expect(accessToken).toBe(tokenGeneratorSpy.accessToken)
    expect(accessToken).toBeTruthy()
  })

  test('Should call UpdateAccessTokenRepository with correct values', async () => {
    const { sut, loadUserByEmailRepositorySpy, updateAccessTokenRepositorySpy, tokenGeneratorSpy } = makeSut()
    await sut.auth('valid@email.com', 'anythingHere')
    expect(updateAccessTokenRepositorySpy.userId).toBe(loadUserByEmailRepositorySpy.user.id)
    expect(updateAccessTokenRepositorySpy.accessToken).toBe(tokenGeneratorSpy.accessToken)
  })

  test('Should throws if invalid dependencies are provided', async () => {
    class LoadUserByEmailRepositorySpy { }
    const loadUserByEmailRepositorySpy = new LoadUserByEmailRepositorySpy()

    class EncrypterSpy { }
    const encrypterSpy = new EncrypterSpy()

    class TokenGeneratorSpy { }
    const tokenGeneratorSpy = new TokenGeneratorSpy()

    class UpdateAccessTokenRepositorySpy { }
    const updateAccessTokenRepositorySpy = new UpdateAccessTokenRepositorySpy()

    const suts = [].concat(
      new AuthUseCase(),
      new AuthUseCase({
        loadUserByEmailRepository: null,
        encrypter: null,
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: loadUserByEmailRepositorySpy,
        encrypter: null,
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: makeLoadUserByEmailRepository(),
        encrypter: null,
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: makeLoadUserByEmailRepository(),
        encrypter: encrypterSpy,
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: makeLoadUserByEmailRepository(),
        encrypter: makeEncrypter(),
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: makeLoadUserByEmailRepository(),
        encrypter: makeEncrypter(),
        tokenGenerator: tokenGeneratorSpy,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: makeLoadUserByEmailRepository(),
        encrypter: makeEncrypter(),
        tokenGenerator: makeTokenGenerator(),
        updateAccessTokenRepository: updateAccessTokenRepositorySpy
      })
    )

    for (const sut of suts) {
      const promise = sut.auth('anyemail@email.com', 'anythingHere')
      await expect(promise).rejects.toThrow(new ServerError())
    }
  })

  test('Should throws if any depencies throws', async () => {
    const loadUserByEmailRepositoryWithErrorSpy = makeLoadUserByEmailRepositoryWithError()
    const encrypterWithErrorSpy = makeEncrypterWithError()
    const tokenGeneratorWithErrorSpy = makeTokenGeneratorWithError()
    const updateAccessTokenRepositoryWithErrorSpy = makeUpdateAccessTokenRepositoryWithError()

    const { loadUserByEmailRepositorySpy, encrypterSpy, tokenGeneratorSpy } = makeSut()

    const suts = [].concat(
      new AuthUseCase({
        loadUserByEmailRepository: loadUserByEmailRepositoryWithErrorSpy,
        encrypter: null,
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: loadUserByEmailRepositorySpy,
        encrypter: encrypterWithErrorSpy,
        tokenGenerator: null,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: loadUserByEmailRepositorySpy,
        encrypter: encrypterSpy,
        tokenGenerator: tokenGeneratorWithErrorSpy,
        updateAccessTokenRepository: null
      }),
      new AuthUseCase({
        loadUserByEmailRepository: loadUserByEmailRepositorySpy,
        encrypter: encrypterSpy,
        tokenGenerator: tokenGeneratorSpy,
        updateAccessTokenRepository: updateAccessTokenRepositoryWithErrorSpy
      })
    )

    for (const sut of suts) {
      const promise = sut.auth('anyemail@email.com', 'anythingHere')
      await expect(promise).rejects.toThrow()
    }
  })
})
