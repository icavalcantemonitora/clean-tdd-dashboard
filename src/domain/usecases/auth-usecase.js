const { MissingParamError, ServerError } = require('../../utils/errors')

class AuthUseCase {
  constructor ({ loadUserByEmailRepository, encrypter, tokenGenerator, updateAccessTokenRepository } = {}) {
    this.loadUserByEmailRepository = loadUserByEmailRepository
    this.encrypter = encrypter
    this.tokenGenerator = tokenGenerator
    this.updateAccessTokenRepository = updateAccessTokenRepository
  }

  async auth (email, password) {
    if (!email) {
      throw new MissingParamError('email')
    }
    if (!password) {
      throw new MissingParamError('password')
    }
    if (!this.loadUserByEmailRepository) {
      throw new ServerError()
    }
    if (!this.loadUserByEmailRepository.load) {
      throw new ServerError()
    }
    if (!this.encrypter) {
      throw new ServerError()
    }
    if (!this.encrypter.compare) {
      throw new ServerError()
    }
    if (!this.tokenGenerator) {
      throw new ServerError()
    }
    if (!this.tokenGenerator.generate) {
      throw new ServerError()
    }
    if (!this.updateAccessTokenRepository) {
      throw new ServerError()
    }
    if (!this.updateAccessTokenRepository.update) {
      throw new ServerError()
    }
    const user = await this.loadUserByEmailRepository.load(email)
    const isValid = user && await this.encrypter.compare(password, user.password)

    if (isValid) {
      const accessToken = await this.tokenGenerator.generate(user.id)
      await this.updateAccessTokenRepository.update(user.id, accessToken)
      return accessToken
    }
    return null
  }
}

module.exports = AuthUseCase
