const { ServerError } = require('../../utils/errors')

class UpdateAccessTokenRepository {
  constructor (userModel) {
    this.userModel = userModel
  }

  async update (userId, accessToken) {
    if (!userId) {
      throw new ServerError()
    }

    if (!accessToken) {
      throw new ServerError()
    }
    await this.userModel.updateOne({
      _id: userId
    },
    {
      $set: {
        accessToken
      }
    })
  }
}

module.exports = UpdateAccessTokenRepository
