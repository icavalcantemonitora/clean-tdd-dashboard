const { ServerError } = require('../../utils/errors')

class LoadUserByEmailRepository {
  constructor (userModel) {
    this.userModel = userModel
  }

  async load (email) {
    if (!email) {
      throw new ServerError()
    }
    const user = await this.userModel.findOne({
      email
    }, {
      projection: {
        password: 1
      }
    })

    return user
  }
}

module.exports = LoadUserByEmailRepository
