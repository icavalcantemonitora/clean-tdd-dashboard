const { ServerError } = require('../../utils/errors')
const MongoHelper = require('../helper/mongo-helper')
const UpdateAccessTokenRepository = require('./update-access-token-repository')
let userModel
let fakeUserId

const makeSut = () => {
  const sut = new UpdateAccessTokenRepository(userModel)

  return { sut, userModel }
}

describe('Update Access Token Repository', () => {
  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL)
    userModel = await MongoHelper.getCollection('users')
  })

  beforeEach(async () => {
    await userModel.deleteMany()
    const fakeUser = await userModel.insertOne({
      email: 'valid_email@mail.com',
      name: 'any_name',
      age: 'any_age',
      state: 'any_state',
      password: 'any_password'
    })
    fakeUserId = fakeUser.ops[0]._id
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  test('Should update the user with the given accessToken', async () => {
    const { sut, userModel } = makeSut()

    await sut.update(fakeUserId, 'valid_token')
    const updatedFakeUser = await userModel.findOne({
      _id: fakeUserId
    })
    expect(updatedFakeUser.accessToken).toBe('valid_token')
  })

  test('Should throw if no userModel is provided', async () => {
    const sut = new UpdateAccessTokenRepository()

    const promise = sut.update(fakeUserId, 'valid_token')
    await expect(promise).rejects.toThrow()
  })

  test('Should throw if no id is provided', async () => {
    const { sut } = makeSut()

    const promise = sut.update()
    await expect(promise).rejects.toThrow(new ServerError())
  })

  test('Should throw if no accessToken is provided', async () => {
    const { sut } = makeSut()

    const promise = sut.update(fakeUserId)
    await expect(promise).rejects.toThrow(new ServerError())
  })
})
