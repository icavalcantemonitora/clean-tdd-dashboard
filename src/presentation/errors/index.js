const UnauthorizedError = require('./unauthorized-error')

module.exports = {
  UnauthorizedError
}
