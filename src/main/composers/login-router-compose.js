const LoginRouter = require('../../presentation/routers/login-router')
const AuthUserCase = require('../../domain/usecases/auth-usecase')
const EmailValidator = require('../../utils/email-validator')
const LoadUserByEmailRepository = require('../../infra/repositories/load-user-by-email-repository')
const UpdateAccessTokenRepository = require('../../infra/repositories/update-access-token-repository')
const Encrypyter = require('../../utils/encrypter')
const TokenGenerator = require('../../utils/token-generator')
const MongoHelper = require('../../infra/helper/mongo-helper')
const env = require('../config/env')
const loginRouter = async () => {
  const userModel = await MongoHelper.getCollection('users')
  const encrypyter = new Encrypyter(env.tokenSecret)
  const tokenGenerator = new TokenGenerator()
  const loadUserByEmailRepository = new LoadUserByEmailRepository(userModel)
  const updateAccessTokenRepository = new UpdateAccessTokenRepository(userModel)
  const authUseCase = new AuthUserCase({
    loadUserByEmailRepository,
    updateAccessTokenRepository,
    encrypyter,
    tokenGenerator
  })
  const emailValidator = new EmailValidator()
  return new LoginRouter({ authUseCase, emailValidator })
}

module.exports = loginRouter
