const express = require('express')
const app = express()
const setupApp = require('./setup')
const setUpRoutes = require('./routes')

setupApp(app)
setUpRoutes(app)

module.exports = app
