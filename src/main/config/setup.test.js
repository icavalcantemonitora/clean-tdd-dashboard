const request = require('supertest')
const app = require('./app')

describe('App Setup', () => {
  test('Should desable x-powered-by header', async () => {
    app.get('/test_x_powered_by', (req, res) => {
      res.send({}, 200)
    })
    const response = await request(app)
      .get('/')

    expect(response.headers['x-powered-by']).toBeUndefined()
  })
})
