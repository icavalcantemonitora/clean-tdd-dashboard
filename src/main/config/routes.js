const router = require('express').Router()
const fb = require('fast-glob')

module.exports = async (app) => {
  app.use('/api', router)
  const files = fb.sync('**/src/main/routes/**.js')

  for (const file of files) {
    const route = require(`../../../${file}`)
    await route(router)
  }
}
