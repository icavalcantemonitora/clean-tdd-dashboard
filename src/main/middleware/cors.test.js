const request = require('supertest')
const app = require('../config/app')

describe('CORS Middleware', () => {
  test('Should enable CORS', async () => {
    app.get('/test_cors', (req, res) => {
      res.status(200).send({})
    })
    const response = await request(app)
      .get('/test_cors')

    expect(response.headers['access-control-alow-origin']).toBe('*')
    expect(response.headers['access-control-alow-methods']).toBe('*')
    expect(response.headers['access-control-alow-headers']).toBe('*')
  })
})
